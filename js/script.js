// Lancement de la fenêtre
window.onload = function() {
    
// création du canvas : elt html permettant de dessiner
    var canvas =  this.document.createElement("canvas");
    canvas.width = 900;
    canvas.height = 600;
    canvas.style.border = "1px solid";

// lier le canvas au fichier html
    document.body.appendChild(canvas);

// dessiner
    // contexte 2d
    var ctx = canvas.getContext("2d");
    // couleur du contexte : rouge
    ctx.fillStyle = "#ff0000";
    // création d'un rectangle avec 4 arguements en px : 
        // x(position horizontale) 
        // y(position verticale) 
        // largeur
        // hauteur
    ctx.fillRect(30,30,100,50)

}